#!/bin/sh
#scl enable rh-ruby23 bash

nohup webhook -port 9999 -secure -cert /etc/pki/tls/certs/star.vcube.net.crt -key /etc/pki/tls/private/star.vcube.net.key --verbose > webhook.log &
