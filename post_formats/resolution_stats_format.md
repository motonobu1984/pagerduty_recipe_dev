###  [Incident %<incident_id>s ](%<incident_url>s) has been Resolved

**Summary** %<incident_summary>s 

**Resolution Time:** %<resolution_time>s

**Resolution Reason:** %<resolve_reason>s