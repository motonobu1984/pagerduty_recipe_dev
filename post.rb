#!/opt/rh/rh-ruby23/root/usr/bin/ruby

require './pagerduty_api.rb'
require './mattermost_api.rb'

$config = YAML.load(
	File.open('conf.yaml').read
)

api = PagerdutyApi.new($config['pagerduty_api']['url'],
						$config['pagerduty_api']['mailaddress'],
						$config['pagerduty_api']['api_token'])

mattermost = MattermostApi.new($config['mattermost_api']['url'],
                                                $config['mattermost_api']['login_id'],
                                                $config['mattermost_api']['password'])

if !ARGV[0]['context'].nil?
        messages = JSON.parse(ARGV[0])['context']
        api.add_post(messages)
elsif !ARGV[0]['trigger_word'].nil?
        messages = JSON.parse(ARGV[0])
        api.add_note(messages) 
elsif !ARGV[0]['text'].nil?
        message = JSON.parse(ARGV[0])
        mattermost.open_dialog(message)
else
	message = JSON.parse(ARGV[0])
        api.add_note_from_dialog(message)	
end
