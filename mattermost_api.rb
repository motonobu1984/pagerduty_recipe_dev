require 'httparty'
require 'time'


class MattermostApi
        include HTTParty

        format :json
        # debug_output $stdout

        def initialize(mattermost_url, login_id, password)
                @base_uri = mattermost_url + 'api/v4/'
                @login_id = login_id
                @password = password

                @options = {
                        headers: {
                                'Content-Type' => 'application/json',
                                'User-Agent' => 'Mattermost-HTTParty'
                        },
                        # TODO Make this more secure
                        verify: false
                }

                login_options = @options
                login_options[:headers]['Content-Type' => 'application/x-www-form-urlencoded']
                login_options[:body] = {'login_id' => @login_id, 'password' => @password}.to_json

                token = self.class.post("#{@base_uri}users/login", login_options).headers['Token']

                @options[:headers]['Authorization'] = "Bearer #{token}"
                @options[:body] = nil
        end

        def test_api
                JSON.parse(self.class.get("#{@base_uri}users/me", @options).to_s)
        end

        def open_dialog(message)
          post_data ={
                          "trigger_id":message['trigger_id'],
                          "url": "https://sg-webhook.vcube.net:9999/hooks/pagerduty_post",
                          #"icon_url": "http://www.mattermost.org/wp-content/uploads/2016/04/icon.png",
                          "icon_url": "https://d25hn4jiqx5f7l.cloudfront.net/companies/logos/original/pagerduty_1501190197.png",
                          "dialog":{
                             "title": "Note",
                             "callback_id": "1",
                             "submitlabel": "submit",
                             "elements": [
                                                {
                                                "display_name": "Incident ID",
                                                "name": "incident",
                                                "Type":        "text",
                                                "Default":     "Incident ID"
                                                },
                                                {
                                                display_name: "Note",
                                                Name:        "note",
                                                Type:        "text"
                                                },
                                                {
                                                display_name: "User Name",
                                                Name:        "user_name",
                                                Type:        "text",
                                                Default:     "Mattermost"
                                                }
                             ],
                             "notify_on_cancel": true
                            },
                    }
                         options = @options
                         options[:headers]['Content-Type' => 'application/x-www-form-urlencoded']
                         options[:body] = post_data.to_json
                         response = self.class.post("#{@base_uri}actions/dialogs/open", options)
                         return response.code
        end

        def get_incident_channel_id(incident, team_name)
                team_id = self.get_team_by_name(team_name)

                channel_name = self.make_channel_name(incident)

                if self.channel_exists?(channel_name, team_id)
                        channel_id = get_channel_id(channel_name, team_id)
                else
                        channel_id = create_channel(incident, team_id)
                end

                return channel_id
        end

        def get_team_by_name(team_name)
                self.class.get("#{@base_uri}teams/name/#{team_name}", @options)['id']
        end

        def channel_exists?(channel_name, team_id)
                return !get_channel_id(channel_name, team_id).nil?
        end

        def get_channel_id(channel_name, team_id)
                channel = self.class.get("#{@base_uri}teams/#{team_id}/channels/name/#{channel_name}", @options)

                if !channel['name'].nil? and channel['name'] == channel_name
                        return channel['id']
                else
                        return nil
                end
        end

        def create_channel(incident, team_id)
                data = {name: self.make_channel_name(incident),
                                display_name: "PagerDuty Incident #{incident['id']}",
                                team_id: team_id,
                                type: 'P'}.to_json

                options = @options
                options[:body] = data

                channel = self.class.post("#{@base_uri}channels", options)

                return channel['id']
        end

        def get_user_from_username(username)
                self.class.get("#{@base_uri}users/username/#{username}", @options)
        end

        def add_user_to_channel(username, channel_id)
                user_id = self.get_user_from_username(username)['id']

                options = @options
                options[:body] = {user_id: user_id}.to_json

                self.class.post("#{@base_uri}channels/#{channel_id}/members", options)
        end

        def make_channel_name(incident)
                "incident-#{incident['id'].downcase}"
        end

        def update_incident_header(incident)
                channel_name = self.make_channel_name(incident)

                incident_data = {
                        incident_id: incident['id'],
                        incident_url: incident['html_url'],
                        incident_summary: incident['summary'],
                        incident_status: incident['status'].capitalize,
                        creation_date: incident['created_at'],
                        last_update: incident['last_status_change_at']
                }

                header_format = File.read('post_formats/header_format.md')
        #post_format = File.read('post_formats/trigger_stats_format.md')
                header_content = header_format % incident_data

            #post_message = post_format % post_data
                #channel_id = self.get_incident_channel_id(incident, $config['notification_settings']['team_name']) #TODO: Pull this from config
        channel_id = $config['notification_settings']['channel_id']
                options = @options
                options[:body] = {id: channel_id, header: header_content }.to_json

                self.class.put("#{@base_uri}channels/#{channel_id}", options)
                #add_post($config['notification_settings']['channel_id'], post_message)
        end

        def remove_user_from_channel(username, channel_id)
                user_id = self.get_user_from_username(username)['id']

                options = @options
                options[:body] = {user_id: user_id, channel_id: channel_id}.to_json

                self.class.delete("#{@base_uri}channels/#{channel_id}/members/#{user_id}", options)
        end

        def add_post(channel, message)
                team_id = team_id = self.get_team_by_name($config['notification_settings']['team_name'])
                id = $config['notification_settings']['channel_id']
                channel_id = self.get_channel_id(id, team_id)

                options = @options
                options[:body] = {channel_id: channel_id, message: message}.to_json

                self.class.post("#{@base_uri}posts", options)
        end

        def post_resolution_statistics(incident)
                post_format = File.read('post_formats/resolution_stats_format.md')

                # Resolution time is incident['last_status_change_at'] incident['created_at']
                change_t = Time.parse(incident['last_status_change_at'])
                create_t = Time.parse(incident['created_at'])

                diff = change_t - create_t

                resolution_time = formatted_duration(diff.to_i)

                post_data = {
                        incident_summary: incident['summary'],
                        incident_id: incident['id'],
                        incident_url: incident['html_url'],
                        resolution_time: resolution_time,
                        resolve_reason: incident['resolve_reason']
                }

                post_message = post_format % post_data

                add_post($config['notification_settings']['channel_id'], post_message)
        end

        def post_incident_message(message,note)
                team_id = team_id = self.get_team_by_name($config['notification_settings']['team_name'])
                id = $config['notification_settings']['channel_id']
                channel_id = self.get_channel_id(id, team_id)
                log_entries = message['log_entries']
                
                attachments = [{
                        title: message['incident']['summary'],
                        title_link: message['incident']['html_url'],
                        color: "#FF0000",
                        "fields": [
                                {
                                  "short":true,
                                  "title":"Urgency",
                                  "value": message['incident']['service']['incident_urgency_rule']['urgency']
                                },
                                {
                                  "short":true,
                                  "title":"Incident Status",
                                  "value": message['incident']['status']
                                },
                                {
                                  "short":true,
                                  "title":"Service",
                                  "value": message['incident']['service']['name']
                                },
                                {
                                  "short":true,
                                  "title":"Incident ID",
                                  "value":  + message['incident']['id']
                                },
                                {
                                  "short":true,
                                  "title":"Integration",
                                  "value": log_entries[0]['agent']['summary'] 
                                },
                                {
                                  "short":true,
                                  "title":"Note",
                                  "value": note
                                }
                          ],
                          "image_url": "https://pagerduty.digitalstacks.net/wp/wp-content/uploads/2019/09/PG-with-DSC-white-logo.png",
                          "actions": [
                                {
                                  "name": "acknowledge",
                                  "integration": {
                                    "url": "https://sg-webhook.vcube.net:9999/hooks/pagerduty_post",
                                    "context": {
                                      "action": "acknowledged",
                                      "id": + message['incident']['id']
                                     }
                                  }
                                },
                                {
                                  "name": "resolve",
                                  "integration": {
                                    "url": "https://sg-webhook.vcube.net:9999/hooks/pagerduty_post",
                                    "context": {
                                      "action": "resolved",
                                      "id": + message['incident']['id']
                                     }
                                  }
                                }
                        ]
                        },
                  ]

                options = @options
                options[:body] = {channel_id: channel_id, "props":{"attachments": attachments}}.to_json

                self.class.post("#{@base_uri}posts", options)
        end

        def post_acknowledge_message(message,note)

                team_id = team_id = self.get_team_by_name($config['notification_settings']['team_name'])
                id = $config['notification_settings']['channel_id']
                channel_id = self.get_channel_id(id, team_id)

		log_entries = message['log_entries']

                attachments = [{
                        title: message['incident']['summary'],
                        title_link: message['incident']['html_url'],
                        color: "#ee7800",
                        "fields": [
                                {
                                  "short":true,
                                  "title":"Urgency",
                                  "value": message['incident']['service']['incident_urgency_rule']['urgency']
                                },
                                {
                                  "short":true,
                                  "title":"Incident Status",
                                  "value": message['incident']['status']
                                },
                                {
                                  "short":true,
                                  "title":"Service",
                                  "value": message['incident']['service']['name']
                                },
                                {
                                  "short":true,
                                  "title":"Incident ID",
                                  "value":  + message['incident']['id']
                                },
                                {
                                  "short":true,
                                  "title":"Integration",
                                  "value": log_entries[0]['agent']['summary'] 
                                },
                                {
                                  "short":true,
                                  "title":"Note",
                                  "value": note
                                }
                          ],
                          "image_url": "https://pagerduty.digitalstacks.net/wp/wp-content/uploads/2019/09/PG-with-DSC-white-logo.png",
                          "actions": [
                                {
                                  "name": "resolve",
                                  "integration": {
                                    "url": "https://sg-webhook.vcube.net:9999/hooks/pagerduty_post",
                                    "context": {
                                      "action": "resolved",
                                      "id": + message['incident']['id']
                                     }
                                  }
                                },
                                {
                                  "name": "add note",
                                  "integration": {
                                    "url": "https://sg-webhook.vcube.net:9999/hooks/pagerduty_post",
                                    "context": {
                                      "action": "addnote",
                                      "id": + message['incident']['id']
                                     }
                                  }
                                }
                        ]
                        },
                  ]

                options = @options
                options[:body] = {channel_id: channel_id, "props":{"attachments": attachments}}.to_json

                self.class.post("#{@base_uri}posts", options)
        end

        def post_resolve_message(message,note)
                team_id = team_id = self.get_team_by_name($config['notification_settings']['team_name'])
                id = $config['notification_settings']['channel_id']
                channel_id = self.get_channel_id(id, team_id)
		
                log_entries = message['log_entries']

                attachments = [{
                        title: message['incident']['summary'],
                        title_link: message['incident']['html_url'],
			color: "#00ed28",
                        "fields": [
                                {
                                  "short":true,
                                  "title":"Urgency",
                                  "value": message['incident']['service']['incident_urgency_rule']['urgency']
                                },
                                {
                                  "short":true,
                                  "title":"Incident Status",
                                  "value": message['incident']['status']
                                },
                                {
                                  "short":true,
                                  "title":"Service",
                                  "value": message['incident']['service']['name']
                                },
                                {
                                  "short":true,
                                  "title":"Incident ID",
                                  "value":  + message['incident']['id']
                                },
                                {
                                  "short":true,
                                  "title":"Integration",
                                  "value": log_entries[0]['agent']['summary']
                                },
                                {
                                  "short":true,
                                  "title":"Note",
                                  "value": note
                                }
                          ],
                          "image_url": "https://pagerduty.digitalstacks.net/wp/wp-content/uploads/2019/09/PG-with-DSC-white-logo.png"
                        },
                  ]

                options = @options
                options[:body] = {channel_id: channel_id, "props":{"attachments": attachments}}.to_json

                self.class.post("#{@base_uri}posts", options)
        end

        # Thanks redbar0n! - https://gist.github.com/shunchu/3175001#gistcomment-2197179
        def formatted_duration(total_seconds)
                days = total_seconds / (60 * 60 * 24)
                hours = total_seconds / (60 * 60)
                minutes = (total_seconds / 60) % 60
                seconds = total_seconds % 60
                [days, hours, minutes, seconds].map do |t|
                        t.round.to_s.rjust(2,'0')
                end.join(':')
        end

       def getaction(event, context)
         print(json.dumps(event))
       end
end
