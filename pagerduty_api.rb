require 'httparty'
require 'time'
require 'json'

class Mattermost
        include HTTParty

        format :json

        def initialize(mattermost_url, login_id, password)
                @base_uri = mattermost_url + 'api/v4/'
                @login_id = login_id
                @password = password

                @options = {
                        headers: {
                                'Content-Type' => 'application/json',
                                'User-Agent' => 'Mattermost-HTTParty'
                        },
                        verify: false
                }

                login_options = @options
                login_options[:headers]['Content-Type' => 'application/x-www-form-urlencoded']
                login_options[:body] = {'login_id' => @login_id, 'password' => @password}.to_json

                token = self.class.post("#{@base_uri}users/login", login_options).headers['Token']

                @options[:headers]['Authorization'] = "Bearer #{token}"
                @options[:body] = nil
        end

        def add_note(messages)
            post_data ={
                          "trigger_id":messages['trigger_id'],
                          "url":"https://mattermost-staging.vcube.sg/api/v4/actions/dialogs/open",
                          "dialog":{
                             "title": "SET",
                             "callback_id": "123",
                             "submitlabel": "submit",
                             "icon_url":"http://www.mattermost.org/wp-content/uploads/2016/04/icon.png",
			     "elements":[
                                        {
					DisplayName: "パスワード",
					Name:        "password",
					Type:        "text",
					SubType:     "password",
            		      },
                             ],
                             "notify_on_cancel": true
   			   },
			}
            options = @options
            options[:headers]['Content-Type' => 'application/x-www-form-urlencoded']
            options[:body] = post_data.to_json
            print post_data
            print options
            response = self.class.post("#{@base_uri}actions/dialogs/open", options)
            print response
        end

end

class PagerdutyApi
        include HTTParty

        format :json
        # debug_output $stdout

        def initialize(pagerduty_url, mailaddress, api_token)
                @base_uri = pagerduty_url
                @mailaddress = mailaddress
                @api_token = api_token

                @options = {
                             headers: {
                                'Content-Type' => 'application/json',
				'accept' => 'application/vnd.pagerduty+json;version=2',
                                'authorization' => 'Token token=' + @api_token,
				'from' => @mailaddress
                             }
                           }

        end

        def get_note(message)
                #id = messages['text'].slice(/P....../)
                id = message['incident']['id']
                @url = @base_uri + 'incidents/' + id + '/notes'
	
		options = @options

		result = self.class.get("#{@url}", options)

                hash = JSON.parse(result.body)
                print hash['notes'][0] 
                
                if !hash['notes'][0].nil?
                  print "**************************************" 
                  print hash['notes']
                  print "**************************************" 
                  note = hash['notes'][0]['content']
                else
                  print "**************************************" 
                  note = ""
                end
                
                return note
        end

        def add_post(messages)

                @url = @base_uri + 'incidents/' + messages['id']

                options = @options
                options[:body] = {"incident": {"type": "incident_reference", "status": messages['action'] }}.to_json
                print options
                print @url
                self.class.put("#{@url}", options)
        end

        def add_note(messages)
                id = messages['text'].slice(/P....../)
                note = messages['text'].slice(/(?<=^.{12}).{1,}/)
                username = messages['user_name']
                @url = @base_uri + 'incidents/' + id + '/notes'
                options = @options
                post_data ={"note":{"content": note + ' @' + username}}
                
                options[:body] = post_data.to_json
                self.class.post("#{@url}", options)
        end

        def add_note_from_dialog(message)
                id = message['submission']['incident']
                note = message['submission']['note'] 
                username = message['submission']['user_name']
                @url = @base_uri + 'incidents/' + id + '/notes'
                options = @options
                post_data ={"note":{"content": note + ' @' + username}}
                options[:body] = post_data.to_json
                self.class.post("#{@url}", options)
        end

        def getaction(messages, context)
          print(json.dumps(messages))
        end
end
